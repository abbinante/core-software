#include <stdio.h>
#include <unistd.h>

#include <string.h>

void usage()
{
	fprintf(stderr, "Usage: ln [-s] origin dest");
}

void createsoft(char *origin, char *dest)
{
	if (symlink(origin, dest) == -1)
		printf("symlink failed\n");
}

void createhard(char *origin, char *dest)
{
	if (link(origin, dest) == -1)
		printf("hardlink failed\n");
}

int main(int argc, char *argv[])
{
	if (argc < 3)
		usage();

	else {
		if (strncmp(argv[1], "-s", 2) == 0)
			createsoft(argv[2], argv[3]);

		else
			createhard(argv[1], argv[2]);
	}
}
