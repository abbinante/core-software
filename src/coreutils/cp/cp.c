#include <stdio.h>
#include <sys/stat.h>

#include "copy.h"
#include "is.h"

int main(int argc, char *argv[])
{
	if (argc < 2)
		printf("Usage: cp [-r] origin dest\n");

	else {
		if (is_directory(argv[1]))
			printf("%s is a directory\n", argv[1]);

		else
			copy_file(argv[1], argv[2]);
	}

	return 0;
}
