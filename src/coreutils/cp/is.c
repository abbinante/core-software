#include <stdio.h>
#include <sys/stat.h>

short is_directory(char *directory)
{
	struct stat directory_stat;
	stat(directory, &directory_stat);

	return S_ISDIR(directory_stat.st_mode);
}
