#include <stdio.h>

void copy_file(char *origin, char *dest)
{
	char content[512];

	FILE *input = fopen(origin, "r");
	FILE *output = fopen(dest, "w");

	while (fgets(content, sizeof(content), input) != NULL)
		fputs(content, output);
}
