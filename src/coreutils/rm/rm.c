#include <stdio.h>
#include <stdlib.h>

#include <string.h>
#include <sys/stat.h>

#include <ftw.h>
#include <unistd.h>

short recursive;

void usage()
{
	fprintf(stderr, "Usage: rm [-r] file\n");
}

static int rmfile(const char *pathname, const struct stat *sbuf, int type, struct FTW *ftwb)
{
	if (remove(pathname) < 0)
		return -1;

	return 0;
}

short rmdirectory(char *dir)
{
	if (nftw(&dir[0], rmfile, 10, FTW_DEPTH|FTW_MOUNT|FTW_PHYS) < 0)
		return 1;

	return 0;
}

int main(int argc, char *argv[])
{
	short i;
	short opt;

	/* reset flags */
	recursive = 0;

	/* if no arguments are given then display usage */
	if (argc == 1)
		usage();

	/* process options */
	while ((opt = getopt(argc, argv, "rf")) != -1) {
		switch (opt) {
		case 'r':
			recursive = 1;
			break;

		/* alias for -r due to retards using -f in makefiles are being too stupid to know what the fuck it does */
		case 'f':
			recursive = 1;
			break;

		default:
			usage();
		}
	}

	/* adjust argc and argv so that only the files and directories are left */
	argc -= optind;
	argv += optind;

	/* get files and directories for deletion */
	for (i = 0; i < argc; i++) {
		char *target = argv[i];
		short ret = recursive ? rmdirectory(target) : remove(target);

		if (ret != 0) {
			usage();
			perror(NULL);
		}
	}

	return 0;
}
