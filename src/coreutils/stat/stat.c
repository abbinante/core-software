#include <stdio.h>
#include <sys/stat.h>

/* TODO:
 * convert the following to human readable:
 * mode
 * uid
 * gid
 * size
 *
 * Output should look like:
 * 1050 1846088 -rw-r--r-- 1 user user 7362775 420 "Oct 21 00:54:34 2019" "Oct 21 00:54:34 2019" "Oct 21 00:54:34 2019" 16384 4 0 stat.c
 */

int main(int argc, char *argv[])
{
	short i;
	struct stat st;

	for (i = 0; i < argc; i++) {
		if(stat(argv[i], &st) != 0)
			fprintf(stderr, "file not found\n");

		else
			printf("%d %llu %u %u %u %u %d %lld %lld %d %u %u", st.st_dev, st.st_ino, st.st_mode, st.st_nlink, st.st_uid, st.st_gid, st.st_rdev, st.st_size, st.st_blocks, st.st_blksize, st.st_flags, st.st_gen);
	}
}
