#include <stdio.h>
#include <sys/stat.h>

int main(int argc, char *argv[])
{
	short i;
	struct stat directory = {0};

	for (i = 1; i < argc; i++) {
		/* check if directory already exists */
		if (stat(argv[i], &directory) != -1)
			printf("%s already exists", argv[i]);

		else
			mkdir(argv[i], 0777);
	}
}
