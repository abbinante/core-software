#include <stdio.h>
#include <stdlib.h>

#include <string.h>
#include <unistd.h>

#include "create.h"
#include "add.h"

#include "check.h"

void usage(char *prog_name)
{
	fprintf(stderr, "Usage: %s [-ad]\n", prog_name);
	exit(-1);
}

int main(int argc, char *argv[])
{
	short i;
	void (*action)(char *);

	if (argc < 3)    /* you need to give at least 3, 0: exec name, 1: dash, 2: options, 3+: package(s) */
		usage(argv[0]);

	else {
/*		check_for_directory("/tmp/creep");
		chdir("/tmp/creep"); */

		if (strncmp(argv[1], "-a", 2) == 0)
			action = &add;

		else
			usage(argv[0]);

		for (i = 2; i < argc; i++)
			action(argv[i]);
	}

	return 0;
}
