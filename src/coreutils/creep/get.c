#include <stdio.h>
#include <openssl/ssl.h>

#include <openssl/bio.h>
#include <openssl/err.h>

#include "config.h"

extern BIO *bio;
extern BIO *out;

void get_package(char *package, char *filename)
{
	short i;
	char *error_code;

	char contents[1536];

	/* create GET request */
	if (BIO_printf(bio, "GET %s HTTP/1.1\r\nHost: %s\r\n\r\n", package, HOST[0]) == -1)
		fprintf(stderr, "could not create get request");

	BIO_read(bio, contents, sizeof(contents));

	/* check for 404 error */
	for (i = 0; i < 23; i++)
		sprintf(error_code, "%c", contents[i]);

	if (strncmp(error_code, "HTTP/1.1 404 Not Found", 24))
		fprintf(stderr, "404 not found\n");

	/* create file for contents */
	out = BIO_new_file(filename, "w");

	if (!out) {
		fprintf(stderr, "unable to open file %s\n", filename);
		ERR_print_errors_fp(stderr);
		exit(-1);
	}
}
