#include <stdio.h>
#include <string.h>

#include <stdlib.h>

#include "check.h"
#include "connection.h"

#include "parse.h"
#include "config.h"

#include "get.h"
#include "create.h"

void add(char *package)
{
	if (check_for_package(package) == 1)
		exit(1);

	else {
		char packagepath[512] = {0};
		char directory_path[512];

		char install_path[512];
		char *filename;

		/* get package */
		make_connection();
		create_path(package, packagepath);

		filename = parse_name(package);
		get_package(packagepath, filename);

		close_connection(); 

		/* get creep build */
		make_connection();
		create_path(package, packagepath);

		strcat(packagepath, ".creep");
		filename = parse_name(packagepath);

		get_package(packagepath, filename);
		close_connection();
	}
}
