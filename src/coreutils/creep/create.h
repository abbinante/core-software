#ifndef CREATE_H
#define CREATE_H

void create_directory(char *directory);
void create_path(char *package, char *path);

void create_symlink(char *origin, char *destination);
void create_destination(char *filename);

#endif
