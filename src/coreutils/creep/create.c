#include <stdio.h>
#include <sys/stat.h>

#include <string.h>
#include <unistd.h>

#include "check.h"
#include "config.h"

#include "parse.h"

void create_path(char *package, char *path)
{
	strcpy(path, PAGE[0]);
	strcat(path, package);
}

void create_symlink(char *origin, char *destination)
{
	if (symlink(origin, destination) == -1)
		printf("creating symlink failed\n");
}

void create_destination(char *filename)
{
	char provider[512];
	char package_name[512];

	char directory[512];
	char description[512];

	char req_depends[512];
	char opt_depends[512];

	char version[512];
	char path[512];

	parsecreep(filename, provider, package_name, directory, description, req_depends, opt_depends, version);

	strcpy(path, "/opt/");
	strcat(path, provider);

	printf("%s\n", path);

	strcat(path, "/");
	strcat(path, package_name);

	printf("%s\n", path);
}
