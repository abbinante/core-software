#include <stdio.h>
#include <string.h>

#include <stdlib.h>

char *parse_name(char *path)
{
	const char delminiter = '/';
	char *filename = path;

	while (*path != '\0') {
		if (*path == delminiter) {
			filename = path + 1;
		}
		path++;
	}

	return filename;
}

void parsecreep(char *filename, char provider[], char package_name[], char directory[], char description[], char req_depends[], char opt_depends[], char version[])
{
	char contents[512];
	short line = 0;

	FILE *file;
	file = fopen(filename, "r");

	if (file == 0) {
		printf("couldn't open file\n");
		exit(-1);
	}

	else {
		while(fgets(contents, sizeof(contents), file) != NULL) {
			line++;

			if (line == 1)
				strcpy(provider, contents);

			else if (line == 2)
				strcpy(package_name, contents);

			else if (line == 3)
				strcpy(directory, contents);

			else if (line == 4)
				strcpy(req_depends, contents);

			else if (line == 5)
				strcpy(opt_depends, contents);

			else if (line == 6)
				strcpy(version, contents);
		}
	}

	fclose(file);
}
