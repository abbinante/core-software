#include <stdio.h>
#include <stdlib.h>

#include <unistd.h>
#include <sys/stat.h>

short check_for_package(char *package)
{
	short i;
	char directory[6][51] = { "/bin/", "/sbin/", "/opt/bin/", "/usr/bin/", "/usr/sbin/", "/usr/games/" };

	struct stat *program = calloc(1, sizeof(struct stat));

	for (i = 0; i < 6; i++) {
		if (chdir(directory[i]) != 0)
			printf("%s - could not access directory\n", directory[i]);

		if (stat(package, program) == 0) {
			printf("%s is already installed\n", package);
			return 1;
		}
	}

	return 0;
}

void check_for_directory(char *directory)
{
	struct stat dir = {0};

	if (stat(directory, &dir) == -1)
		mkdir(directory, 0777);
}
