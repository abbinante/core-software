#include <stdio.h>
#include <stdlib.h>

#include <string.h>
#include <stdbool.h>

#include <sys/stat.h>
#include <dirent.h>

#include <errno.h>

void determine_type(struct stat stat_type, char *filename)
{
	/*                           black (0)     red (1)       green (2)     yellow (3)    blue (4)      magenta (5)   cyan (6)      white (7)       reset (16) */
	const char colors[17][11] = { "\033[0;30m", "\033[0;31m", "\033[0;32m", "\033[0;33m", "\033[0;34m", "\033[0;35m", "\033[0;36m", "\033[0;37m",
	                              "\033[1;30m", "\033[1;31m", "\033[1;32m", "\033[1;33m", "\033[1;34m", "\033[1;35",  "\033[1;36m", "\033[1;37m",  "\033[0m" };

	if (S_ISDIR(stat_type.st_mode)) {
		printf("%s", colors[9]);
		printf("%s\n", filename);
		printf("%s", colors[16]);
		return;
	}

	if (S_ISREG(stat_type.st_mode)) {
		printf("%s", colors[2]);
		printf("%s\n", filename);
		printf("%s", colors[16]);
		return;
	}

	if (S_ISFIFO(stat_type.st_mode)) {
		printf("%s", colors[10]);
		printf("%s\n", filename);
		printf("%s", colors[16]);
		return;
	}

	if (S_ISCHR(stat_type.st_mode)) {
		printf("%s", colors[8]);
		printf("%s\n", filename);
		printf("%s", colors[16]);
		return;
	}

	if (S_ISBLK(stat_type.st_mode)) {
		printf("%s", colors[5]);
		printf("%s\n", filename);
		printf("%s", colors[16]);
		return;
	}

	if (S_ISLNK(stat_type.st_mode)) {
		printf("%s", colors[1]);
		printf("%s\n", filename);
		printf("%s", colors[16]);
		return;
	}

	printf("%s\n", filename);
	return;
}

bool hidden(char *filename)
{
	return (strncmp(filename, ".", strlen(".")) == 0);
}

int main(int argc, char *argv[])
{
	char *dirname = ".";
	bool aFlag = false;

	if (argc >= 2) {
		dirname = argv[argc - 1];

		/* show hidden files if -a is passed */
		if (strcmp(argv[1], "-a") == 0) {
			aFlag = true;

			if (argc == 2)
				dirname = ".";
		}
	}

	struct stat file_type;
	struct dirent *contents = NULL;

	DIR *directory = opendir(dirname);

	if (directory == NULL) {
		perror("couldn't open directory\n");
		exit(1);
	}

	while ((contents = readdir(directory)) != NULL) {
		if ((hidden(contents->d_name) && !aFlag) || (hidden(contents->d_name) && aFlag))
			continue;

		stat(contents->d_name, &file_type);
		determine_type(file_type, contents->d_name);
	}

	closedir(directory);
}
