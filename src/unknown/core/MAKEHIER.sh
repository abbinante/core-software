#!/bin/sh

# Copyright 2019 Core Team (https://gitlab.com/Puffles_the_Dragon/core)

# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

test -z $1 && echo "Usage: $0 target" && exit


read -n1 -p "Are you sure you want to create a new hier on $1? " user
echo
echo

test -z $user && echo Aborted && exit

if test $user = y
then
   echo Creating directories on $1

   mkdir -pv $1/{bin,boot,dev,etc,lib,media,mnt,opt,sbin,srv,tmp,usr,var}
   mkdir -v $1/{home,root}

   # for historical reasons and because I quite like it create /etc/mtab
   ln -sv /proc/self/mounts $1/etc/mtab

   # Kernel Modules
   mkdir -v $1/lib/modules

   # /opt
   mkdir -v $1/opt/{bin,doc,include,info,lib,man}
   mkdir -v $1/{etc,var}/opt
   # Core dist specific
   mkdir -v $1/opt/creep
   # creep should be in $1/opt/creep/creep (symlinks needed refer to FHS)

   # /usr
   mkdir -v $1/usr{,/local}/{bin,include,lib,local,sbin,share,games,src}
   # /usr/local
   mkdir -v $1/usr/local/etc
   ln -sv /usr/local/etc $1/etc/local # Optional
   
   # /usr/share
   mkdir -pv $1/usr{,/local}/share/{man,misc}
   mkdir -pv $1/usr{,/local}/share/{dict,doc,games,info,locale,nls,terminfo,zoneinfo} # Optional
   ln -sv /usr/local/share/man $1/usr/local/man
   mkdir -pv $1/usr{,/local}/share/man/en/man{1..8}

   # /var
   mkdir -pv $1/var/{cache,lib,local,lock,log,run,spool,tmp}
   mkdir -v $1/var/games # Optional
   mkdir -v $1/var/lib/misc
   touch $1/var/log/{lastlog,messages,wtmp}
   # lastlog  = record of last login of each user
   # messages = system messages from syslogd
   # wtmp     = record of all logins and logouts
      
   echo Done!
else
    echo Aborted.
fi
