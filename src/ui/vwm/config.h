/* appearance */
static const unsigned int borderpx  = 1;        /* border pixel of windows */
static const unsigned int gappx     = 8;        /* gaps between windows */

static const unsigned int snap      = 32;       /* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */

static const int topbar             = 1;        /* 0 means bottom bar */

static const char *fonts[]          = { "Terminus:size=9" };
static const char dmenufont[]       = "Px437 ATI SmallW 6x8:size=8";

static const char black[]           = "#000000";
static const char neon_blue[]       = "#00BFFF";

static const char neon_pink[]       = "#FF0099";
static const char neon_green[]      = "#00ff00";

static const char neon_yellow[]     = "#bfff00";
static const char neon_red[]        = "#ff0000";

#define NUMCOLORS 5
static const char *colors[NUMCOLORS][8]      = {
	/* window schemes fg         bg         border   */
	[WindowNorm] = { neon_pink, black, neon_pink },
	[WindowSel]  = { neon_blue, black, neon_blue },

	/* status schemes fg          bg     border */
	[StatusNorm] = { neon_green, black, black },
	[StatusWarn] = { neon_yellow, black, black },

	[StatusUrgent] = { neon_red, black, black },
};

/* tagging                    main    programming asocial media   4     5     6     7     8     9 */
static const char *tags[] = { "主要", "編程", "無社會性", "媒體", "四", "五", "六", "七", "八", "九" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 *
	 * scratchpad:
	 * { .title = "scratchpad: ", .w = , .h =  }
	 */
	/* class           instance         title          tags mask          isfloating         monitor         width         height*/
	{ "Minetest",      NULL,            NULL,          1 << 3,            0,                 -1 },
	{ "mpv",           NULL,            NULL,          1 << 3,            0,                 -1 },

	{ "Tor Browser",   NULL,            NULL,          0,                 1,                 -1 },
	{ "st-256color",   NULL,            NULL,          0,                 0,                 -1,             550,          375 },
};

/* layout(s) */
static const float mfact     = 0.50; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[+]",      grid },
	{ "[單片鏡]", monocle },
	{ "><>",      NULL },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} },

#define AUDIORAISE 0x1008ff13
#define AUDIOLOWER 0x1008ff11
#define AUDIOMUTE  0x1008ff12

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* defines used for scratchpads */
#define PREFIX "scratchpad: "
#define TERMCMD(...) { .v = (const char *[]){ "flux", __VA_ARGS__, NULL } }
#define SCRATCHPAD(NAME, ...) { .name = PREFIX NAME, .cmd = TERMCMD("-t", PREFIX NAME, "-e", __VA_ARGS__) }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", black, "-nf", neon_pink, "-sb", black, "-sf", neon_blue, NULL };

static const char *termcmd[]  = { "flux", NULL };
static const char *power_management[] = { "sh", "-c", "~/.scripts/vwm-power", NULL };

/* web browser */
static const char *browsercmd[] = { "firefox", NULL };
static const char *torbrowsercmd[] = { "tor-browser", NULL };

/* utilities */
static const char *screenshotcmd[] = { "scrot", "-d", "5", "-q", "100", "-e", "mv $f ~/pics/.screenshots/", NULL };
static const char *synthcmd[] = { "synth", NULL };

static const char *popcorncmd[] = { "popcorn", NULL };

/* MPD */
static const char *next_song[] = { "mpc", "-q", "next", NULL };
static const char *past_song[] = { "mpc", "-q", "prev", NULL };

static const char *toggle_song[] = { "mpc", "-q", "toggle", NULL };

/* audio and brightness */
static const char *vol_up[] = { "mixerctl", "outputs.master=+5", NULL };
static const char *vol_down[] = { "mixerctl", "outputs.master=-5", NULL };

static const char *vol_togglemute[] = { "mixerctl", "-t", "inputs.dac-2:3_mute", NULL };

/* scratchpads */
static const Scratchpad scratchpads[] = {
				/* name,      cmd */
	SCRATCHPAD("General",     "vtm"),
	SCRATCHPAD("Python",     "python"),

	SCRATCHPAD("Calculater", "qalc"),
};

static Key keys[] = {
	/* modifier                     key               function            argument */
	{ MODKEY,                       XK_d,             spawn,              {.v = dmenucmd } },
	{ MODKEY,                       XK_Return,        spawn,              {.v = termcmd } },

	{ MODKEY,                       XK_b,             spawn,              {.v = browsercmd } },
	{ MODKEY|ShiftMask,             XK_b,             spawn,              {.v = torbrowsercmd } },

	{ MODKEY,                       XK_m,             spawn,              {.v = synthcmd } },
	{ MODKEY|ShiftMask,             XK_m,             spawn,              {.v = popcorncmd } },

	{ MODKEY,                       XK_Menu,          spawn,              {.v = screenshotcmd } },
	{ MODKEY|ShiftMask,             XK_q,             spawn,              {.v = power_management } },

	{ MODKEY|ShiftMask,             XK_slash,         spawn,              {.v = next_song } },
	{ MODKEY|ShiftMask,             XK_comma,         spawn,              {.v = past_song } },

	{ MODKEY|ShiftMask,             XK_period,        spawn,              {.v = toggle_song } },
	{ 0,                            AUDIORAISE,       spawn,              {.v = vol_up } },

	{ 0,                            AUDIOLOWER,       spawn,              {.v = vol_down } },
	{ 0,                            AUDIOMUTE,        spawn,              {.v = vol_togglemute } },

	{ MODKEY,                       XK_s,             togglebar,          {0} },
	{ MODKEY|ShiftMask,             XK_p,             killclient,         {0} },

	{ MODKEY|ShiftMask,             XK_e,             togglefloating,     {0} },
	{ MODKEY,                       XK_j,             focusstack,         {.i = +1 } },

	{ MODKEY,                       XK_k,             focusstack,         {.i = -1 } },
	{ MODKEY|ControlMask|ShiftMask, XK_bracketleft,   focusmon,           {.i = -1 } },

	{ MODKEY|ControlMask|ShiftMask, XK_bracketright,  focusmon,           {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_bracketleft,   tagmon,             {.i = -1 } },

	{ MODKEY|ShiftMask,             XK_bracketright,  tagmon,             {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_j,             pushdown,           {0} },

	{ MODKEY|ShiftMask,             XK_k,             pushup,             {0} },
	{ MODKEY,                       XK_Tab,           view,               {0} },

	{ MODKEY,                       XK_space,         cyclelayout,        {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_space,         cyclelayout,        {.i = -1 } },

	{ MODKEY|ControlMask,           XK_j,             moveresize,         {.v = (int []){ 0, 25, 0, 0 }}},
	{ MODKEY|ControlMask,           XK_k,             moveresize,         {.v = (int []){ 0, -25, 0, 0 }}},

	{ MODKEY|ControlMask,           XK_l,             moveresize,         {.v = (int []){ 25, 0, 0, 0 }}},
	{ MODKEY|ControlMask,           XK_h,             moveresize,         {.v = (int []){ -25, 0, 0, 0 }}},

	{ MODKEY|ControlMask|ShiftMask, XK_j,             moveresize,         {.v = (int []){ 0, 0, 0, 25 }}},
	{ MODKEY|ControlMask|ShiftMask, XK_k,             moveresize,         {.v = (int []){ 0, 0, 0, -25 }}},

	{ MODKEY|ControlMask|ShiftMask, XK_l,             moveresize,         {.v = (int []){ 0, 0, 25, 0 }}},
	{ MODKEY|ControlMask|ShiftMask, XK_h,             moveresize,         {.v = (int []){ 0, 0, -25, 0 }}},

	{ MODKEY,                       XK_g,             togglescratch,      {.v = &scratchpads[0] } },
	{ MODKEY,                       XK_p,             togglescratch,      {.v = &scratchpads[1] } },

	{ MODKEY,                       XK_c,             togglescratch,      {.v = &scratchpads[2] } },

	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)

	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)

	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)

	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)

	TAGKEYS(                        XK_9,                      8)
	TAGKEYS(                        XK_0,                      9)
};
