void grid()
{
	unsigned int n, cols, rows, cn, rn, i, cx, cy, cw, ch;
	Client *c;

	for (n = 0, c = nextvisible(clients); c; c = nextvisible(c->next), n++)
		;

	if (n == 0)
		return;

	/* grid dimensions */
	for (cols = 0; cols <= n/2; cols++)
		if (cols * cols >= n)
			break;

	/* set layout against the general calculation: not 1:2:2, but 2:3 */
	if (n == 5)
		cols = 2;

	rows = n/cols;

	/* window geometries */
	cw = cols ? waw / cols : waw;
	cn = 0; /* current column number */

	rn = 0; /* current row number */

	for (i = 0, c = nextvisible(clients); c; i++, c = nextvisible(c->next)) {
		if (i/rows + 1 > cols - n%cols)
			rows = n/cols + 1;

		ch = rows ? wah / rows : wah;
		cx = wax + cn*cw;

		cy = way + rn*ch;
		resize(c, cx, cy, cn == cols - 1 ? waw - cx : cw - 1, ch);

	if (rn == 0) {
		if (rn == 0 && cn < cols - 1)
			mvvline(way, cx + cw - 1, 0, wah);

		mvaddch(way, cx + cw - 1, ACS_HLINE);
	}

	rn++;

		if (rn >= rows) {
			rn = 0;
			cn++;
		}
	}
}
