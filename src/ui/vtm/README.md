# TODO
* toggleminized breaks tiling in grid() and minimized stack not shown in monocle()

* fix statusfifo stuffs

* toggled window should keep their position in the stack

* use PREFIX + some key + number of a minized window to move to the minized windows to cycle through them

* maybe remove cmdfifo? Does it do anything for statusfifo? Is it just to run a command with vtm? If so can something like abduco be used without? Maybe nuke this

* do scrollback and copying like tmux

* add status colors

* port over status from VWM

* fix colors (can't show hex spectrum (only 256), it should handle hex colors and 256 colors)

* in monocle mode count the number of windows like in VWM

* windows created with run_command() act weird, they don't seem to be seen as window and break tiling. run_command() needs fixed

* rewrite man page once finished
