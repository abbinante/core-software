/* valid curses attributes are listed below they can be ORed
 *
 * A_NORMAL        Normal display (no highlight)
 *
 * A_STANDOUT      Best highlighting mode of the terminal.
 *
 * A_UNDERLINE     Underlining
 *
 * A_REVERSE       Reverse video
 *
 * A_BLINK         Blinking
 *
 * A_DIM           Half bright
 *
 * A_BOLD          Extra bright or bold
 *
 * A_PROTECT       Protected mode
 *
 * A_INVIS         Invisible or blank mode
 */

enum {
	DEFAULT,
	BLUE,
	PINK,
	RED,
};

static Color colors[] = {
	[DEFAULT] = { .fg = -1,         .bg = -1, .fg256 = -1, .bg256 = -1, },
	[BLUE]    = { .fg = COLOR_BLUE, .bg = -1, .fg256 = 45, .bg256 = -1, },

	[PINK]    = { .fg = PINK, .bg = -1, .fg256 = 198, .bg256 = -1, },
	[RED]     = { .fg = RED, .bg = -1, .fg256 = 160, .bg256 = -1, },
};

#define COLOR(c)        COLOR_PAIR(colors[c].pair)

/* curses attributes for the currently focused window */
#define SELECTED_ATTR   (COLOR(BLUE) | A_NORMAL)

/* curses attributes for normal (not selected) windows */
#define NORMAL_ATTR     (COLOR(PINK) | A_NORMAL)

/* curses attributes for a window with pending urgent flag */
#define URGENT_ATTR     NORMAL_ATTR

/* curses attributes for the status bar */
#define BAR_ATTR        (COLOR(BLUE) | A_NORMAL)

/* status bar (command line option -s) position (BAR_BOTTOM, BAR_OFF) */
#define BAR_POS         BAR_BOTTOM

/* master width factor [0.1 .. 0.9] */
#define MFACT 0.5

/* number of clients in master area */
#define NMASTER 1

/* scroll back buffer size in lines */
#define SCROLL_HISTORY 10000

/* printf format string for the tag in the status bar */
#define TAG_SYMBOL   "%s"

/* curses attributes for the currently selected tags */
#define TAG_SEL      (COLOR(BLUE) | A_BOLD)

/* curses attributes for not selected tags which contain no windows */
#define TAG_NORMAL   (COLOR(PINK) | A_NORMAL)

/* curses attributes for not selected tags which contain windows */
#define TAG_OCCUPIED (COLOR(PINK) | A_NORMAL)

/* curses attributes for not selected tags which with urgent windows */
#define TAG_URGENT (COLOR(RED) | A_NORMAL | A_BLINK)

/* tagging                main    programming asocial media   4     5     6     7     8     9 */
const char tags[][20] = { "主要", "編程", "無社會性", "媒體", "四", "五", "六", "七", "八", "九" };

#include "grid.c"
#include "monocle.c"

#include "deck.c"

/* by default the first layout entry is used */
static Layout layouts[] = {
	{ "[+]", grid },
	{ "[-]", deck },
	{ "[單片鏡]", monocle },
};

#define MOD  CTRL('a')
#define TAGKEYS(KEY,TAG) \
	{ { MOD, ']', KEY,     }, { tag,            { tags[TAG] }               } }, \

/* you can at most specifiy MAX_ARGS (3) number of arguments */
static KeyBinding bindings[] = {
	{ { MOD, 'o',          }, { create,         { NULL }                    } },
	{ { MOD, 'O',          }, { create,         { NULL, NULL, "$CWD" }      } },
	
	{ { MOD, 'j',          }, { focusnext,      { NULL }                    } },
	{ { MOD, 'k',          }, { focusprev,      { NULL }                    } },

	{ { MOD, 'K',          }, { pushup,         { NULL }                    } },
	{ { MOD, 'J',          }, { pushdown,       { NULL }                    } },

	{ { MOD, 'y',          }, { copymode,       { NULL }                    } },
	{ { MOD, 'p',          }, { paste,          { NULL }                    } },

	{ { MOD, CTRL('u'),    }, { scrollback,     { "-1" }                    } },
	{ { MOD, CTRL('d'),    }, { scrollback,     { "1"  }                    } },

	{ { MOD, 'm',          }, { toggleminimize, { NULL }                    } },
	{ { MOD, 's',          }, { togglebar,      { NULL }                    } },

	{ { MOD, 'P',          }, { killclient,     { NULL }                    } },
	{ { MOD, '\t',         }, { viewprevtag,    { NULL }                    } },

	{ { MOD, ' ',          }, { setlayout,      { NULL }                       } },
	{ { MOD, 'Q',          }, { run_command,    { "sh ~/.scripts/vtm-power" }  } },

	{ { MOD, 'b',          }, { create,         { "w3m duckduckgo.com" }           } },
	{ { MOD, 'B',          }, { create,         { "torsocks w3m duckduckgo.com" }  } },

	{ { MOD, '/',          }, { run_command,    { "mpc -q next >/dev/null 2>&1" }  } },
	{ { MOD, ',',          }, { run_command,    { "mpc -q prev >/dev/null 2>&1" }  } },

	{ { MOD, '.',          }, { run_command,    { "mpc -q toggle >/dev/null 2>&1" }               } },
	{ { MOD, '-',          }, { run_command,    { "mixerctl outputs.master=-5 >/dev/null 2>&1" }  } },

	{ { MOD, '=',          }, { run_command,    { "mixerctl outputs.master=+5 >/dev/null 2>&1" }       } },
	{ { MOD, '+',          }, { run_command,    { "mixerctl -t inputs.dac-2:3_mute >/dev/null 2>&1" }  } },

/*	{ { MOD, 'd',          }, { run_command,     { "cmenu_run" }                } }, */ // uncomment when cmenu is ready

	{ { MOD, 'n', '1',     }, { focusn,         { "1" }                     } },
	{ { MOD, 'n', '2',     }, { focusn,         { "2" }                     } },

	{ { MOD, 'n', '3',     }, { focusn,         { "3" }                     } },
	{ { MOD, 'n', '4',     }, { focusn,         { "4" }                     } },

	{ { MOD, 'n', '5',     }, { focusn,         { "5" }                     } },
	{ { MOD, 'n', '6',     }, { focusn,         { "6" }                     } },

	{ { MOD, 'n', '7',     }, { focusn,         { "7" }                     } },
	{ { MOD, 'n', '8',     }, { focusn,         { "8" }                     } },

	{ { MOD, 'n', '9',     }, { focusn,         { "9" }                     } },
	{ { MOD, 'n', '0',     }, { focusn,         { "10" }                    } },

	{ { MOD, 'n', '\t',    }, { focuslast,      { NULL }                    } },

	{ { MOD, '1',          }, { view,           { tags[0] }                 } },
	{ { MOD, '2',          }, { view,           { tags[1] }                 } },

	{ { MOD, '3',          }, { view,           { tags[2] }                 } },
	{ { MOD, '4',          }, { view,           { tags[3] }                 } },

	{ { MOD, '5',          }, { view,           { tags[4] }                 } },
	{ { MOD, '6',          }, { view,           { tags[5] }                 } },

	{ { MOD, '7',          }, { view,           { tags[6] }                 } },
	{ { MOD, '8',          }, { view,           { tags[7] }                 } },

	{ { MOD, '9',          }, { view,           { tags[8] }                 } },
	{ { MOD, '0',          }, { view,           { tags[9] }                 } },

	TAGKEYS( '1',                              0)
	TAGKEYS( '2',                              1)

	TAGKEYS( '3',                              2)
	TAGKEYS( '4',                              3)

	TAGKEYS( '5',                              4)
	TAGKEYS( '6',                              5)

	TAGKEYS( '7',                              6)
	TAGKEYS( '8',                              7)

	TAGKEYS( '9',                              8)
	TAGKEYS( '0',                              9)
};

static const ColorRule colorrules[] = {
	{ "", A_NORMAL, &colors[DEFAULT] }, /* default */
};

static Cmd commands[] = {
	{ "create", { create,	{ NULL } } },
};

/* gets executed when vtm is started */
static Action actions[] = {
	{ create, { NULL } },
};

static char const * const keytable[] = { // MAYBE REMOVE THIS
	/* add your custom key escape sequences */
};
