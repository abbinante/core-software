#include <stdio.h>

void open_file(char *filename)
{
	FILE *file = fopen(filename, "r");
	char content[512];

	while (fgets(content, sizeof(content), file) != NULL)
		printf("%s", content);

	fclose(file);
}

int main(int argc, char *argv[])
{
	open_file(argv[1]);

	return 0;
}
