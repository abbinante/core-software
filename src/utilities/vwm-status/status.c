#include <stdio.h>
#include <unistd.h>

#include <string.h>
#include <time.h>

#include <machine/apmvar.h>
#include <sys/ioctl.h>

#include <fcntl.h>

void get_time(char *time_content)
{
	time_t current_time = time(NULL);
	struct tm time_info = *localtime(&current_time);

	sprintf(time_content, "%d:%d\n", time_info.tm_hour - 4, time_info.tm_min);
}

/* add colors */
void get_battery(char *battery_content)
{
	struct apm_power_info battery;
	short file;

	if ((file = open("/dev/apm", O_RDONLY)) == -1 || ioctl(file, APM_IOC_GETPOWER, &battery) == -1 || close(file) == -1)
		strcpy(battery_content, "電量: 不適用");

	else if (battery.battery_state == APM_BATT_UNKNOWN || battery.battery_state == APM_BATTERY_ABSENT)
		strcpy(battery_content, "電量: 不適用");

	else if (battery.battery_state == APM_BATT_CHARGING)
		sprintf(battery_content, "充電中: %d%%", battery.battery_life);

	else
		sprintf(battery_content, "電量: %d%%", battery.battery_life);
}

void get_volume()
{
}

void get_cpu()
{
}

void get_network()
{
}

void get_song()
{
}

int main()
{
	char *time_content;
	char *battery_content;

	get_time(time_content);
	get_battery(battery_content);

	printf("%s | %s", battery_content, time_content);
}
