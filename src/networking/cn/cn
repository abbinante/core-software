#!/bin/sh

save_nwid=0

while [ $# -gt 0 ]; do
	key="$1"
	case "$key" in
		-s|--save) save_nwid=1 ;;
	esac
	shift
done

kill_interfering() {
	# kill interfering process so we can control network management
	printf "killing interfering process..."
	sudo pkill wpa_supplicant && pkill dhclient
}

setup_interface() {
	# list network interfaces
	iface=$(ip link show | awk '{if (NR % 2 == 1) print $2}' | sed -e 's/://' | awk -F: '{printf "%s, ",$1 }' | sed 's/\(.*\),/\1/')

	# pick interface
	echo "interfaces found: $iface"
	printf "which interface would you like to use? "

	read -r inet

	# check if interface is up or down
	state=$(ip link show "$inet" | awk '{print $9}')

	if [ "$state" != "UP" ]; then
		sudo ip link set "$inet" up
	fi
}

scan_networks() {
	# scan for networks
	printf "scanning available networks..."
	nets=$(sudo iw "$inet" scan | grep 'SSID' | sort -u | awk -F: '/SSID/ {printf "%s\n",$2 }' | sed -e '/^SSID/d' -e '/^[[:space:]]*$/d')
	printf "networks found:\n%s\n" "$nets"
}

is_eth0() {
	# check if $inet is ethernet
	check_ether=$(ifconfig "$inet" | grep "Ethernet" | awk '{print $5}' | sed 's/(//' | sed 's/)//')

	# if $inet is ethernet, then connect to the network
	if [ "$check_ether" = "Ethernet" ]; then
		sudo dhcpcd "$inet" && check_connection
	fi
}

connect_network() {
	# pick SSID and SSID Password
	printf "which network would you like to connect to? "
	read -r ssid

	printf "what is the network password? "
	read -r key

	# save connection details if -s passed
	if [ "$save_nwid" -eq 1 ]; then
		save_network
		wpa_supplicant -B -i "$inet" -c /etc/wpa_supplicant/wpa_supplicant.conf

	else
		temp_network
		wpa_supplicant -B -i "$inet" -c /tmp/wpa_supplicant.conf
	fi
}

save_network() {
	printf 'network {\n
			\tssid="%s"\n
			\tpsk="%s"\n}\n' "${ssid}" "${key}" | sudo tee /etc/wpa_supplicant/wpa_supplicant.conf
}

temp_network() {
	printf 'network {\n
			\tssid="%s"\n
			\tpsk="%s"\n}\n' "${ssid}" "${key}" | sudo tee /tmp/wpa_supplicant.conf
}

check_connection() {
	ping_cmd="ping"
	ping_url="duckduckgo.com"

	printf "testing connection..."
	check_con=$("$ping_cmd" -c 3 "$ping_url" > /dev/null 2>&1)

	if [ "$check_con" -eq 0 ]; then
		printf "connection made\n"
	else
		printf "connection failed\n"
	fi
}

kill_interfering
setup_interface

scan_networks
is_eth0

connect_network
check_connection
